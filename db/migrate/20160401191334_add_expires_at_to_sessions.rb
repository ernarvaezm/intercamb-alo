class AddExpiresAtToSessions < ActiveRecord::Migration
  def change
    add_column :sessions, :expires_at, :timestamp, :default => Time.now
  end
end
