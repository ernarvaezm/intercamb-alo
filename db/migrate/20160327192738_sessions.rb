class Sessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.integer :user_id
      t.string :auth_token
      t.datetime :created_at

      t.timestamps
    end
  end
end
