class User < ActiveRecord::Base
  validates :username, presence: true, length: { maximum: 10 },
            uniqueness: { case_sensitive: false }
  validates :name, :lastname,presence: true
  has_secure_password
  validates :password, length: { minimum: 5 }
end
