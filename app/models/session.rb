class Session < ActiveRecord::Base
  def update
    if self.expires_at > Date.today
      self.expires_at = Time.now + 30.minutes
      self.save
    else
      false
    end
  end
end
