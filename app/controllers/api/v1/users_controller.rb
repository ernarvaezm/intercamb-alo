module Api
  module V1
    class UsersController < ApplicationController
      before_action :authenticate, :checkDate
      before_action :set_user, only: [:show, :edit, :update, :destroy]

      # GET /users
      # GET /users.json
      def index
        @users = User.all
        render json:@users, status: 200
      end

      # GET /users/1
      # GET /users/1.json
      def show
        render json: @user, status: 200
      end

      # GET /users/new
      def new
        @user = User.new
      end

      # GET /users/1/edit
      def edit
      end

      # POST /users
      # POST /users.json
      def create
        @user = User.new(user_params)
          if @user.save
            render json: @user, status: 201
          else
            render json: @user.errors, status:442
          end
      end

      # PATCH/PUT /users/1
      # PATCH/PUT /users/1.json
      def update
        if @user.update(user_params)
          render json: @user, status: 200
        else
          render json: @product.errors, status: :unprocessable_entity
        end
      end

      # DELETE /users/1
      # DELETE /users/1.json
      def destroy
        @user.destroy
        head :no_content
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_user
          @user = User.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def user_params
          params.permit(:name, :lastname, :username, :password)
        end

        def checkDate
          s= Session.find_by(auth_token: @tk)
          unless s.update
            output = {'Error' => 'Your Session Has Timed Out '}.to_json
            render json: output, status:422
          end

        end

        def authenticate
          authenticate_or_request_with_http_token do |token, options|
            @tk=token
            Session.find_by(auth_token: token)
          end
        end
    end
  end
end
