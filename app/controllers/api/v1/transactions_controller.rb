module Api
  module V1
    class TransactionsController < ApplicationController
      before_action :authenticate, :checkDate
      before_action :set_transaction, only: [:show, :edit, :update, :destroy]

      # GET /transactions
      # GET /transactions.json
      def index
        @transactions = Transaction.all
        render json: @transactions, status: 200
      end

      # GET /transactions/new
      def new
        @transaction = Transaction.new
      end

      # POST /transactions
      # POST /transactions.json
      def create
        if valid_products?
          @transaction = Transaction.new(transaction_params)
          if @transaction.save
            render json: @transaction, status: 201
          else
            render json: @transaction.errors, status:442
          end
        else
          render json: "product ID not valid", status:422
        end
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_transaction
        @transaction = Transaction.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def transaction_params

        params.permit(:product_req_id, :product_offered_id)
      end
      def valid_products?

        Product.find_by(id: params[:product_req_id]).present? && Product.find_by(id: params[:product_offered_id]).present?
      end

      def checkDate
        s= Session.find_by(auth_token: @tk)
        unless s.update
          output = {'Error' => 'Your Session Has Timed Out '}.to_json
          render json: output, status:422
        end
      end

      def authenticate
        authenticate_or_request_with_http_token do |token, options|
          @tk=token
          Session.find_by(auth_token: token)
        end
      end
    end
  end
end

