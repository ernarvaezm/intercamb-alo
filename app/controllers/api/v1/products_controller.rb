module Api
  module V1
    class ProductsController < ApplicationController
      before_action :authenticate, :checkDate
      before_action :set_product, only: [:show, :edit, :update, :destroy]
      #
      # def search
      #
      # end

      # GET /products
      # GET /products.json
      def index
        @products = Product.all
        render json: @products, status: 200
      end

      # GET /products/1
      # GET /products/1.json
      def show
        render json: @product, status: 200
      end

      # GET /products/new
      def new
        @product = Product.new
      end

      # GET /products/1/edit
      # def edit
      # end

      # POST /products
      # POST /products.json
      def create
        @product = Product.new(product_params)
          if @product.save
            render json: @product, status: 201
          else
            render json: @product.errors, status:442
          end
      end

      # PATCH/PUT /products/1
      # PATCH/PUT /products/1.json
      def update
        if @product.update(product_params)
          render json: @product, status: 200
        else
          render json: @product.errors, status: :unprocessable_entity
        end
      end

      # DELETE /products/1
      # DELETE /products/1.json
      def destroy
        @product.destroy
        head :no_content
      end


      private
        # Use callbacks to share common setup or constraints between actions.
        def set_product
          @product = Product.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def product_params
          params.permit(:name, :description, :state)
        end

      def checkDate
        s= Session.find_by(auth_token: @tk)
        unless s.update
          output = {'Error' => 'Your Session Has Timed Out '}.to_json
          render json: output, status:422
        end
      end

      def authenticate
        authenticate_or_request_with_http_token do |token, options|
          @tk=token
          Session.find_by(auth_token: token)
        end
      end
    end
  end
end