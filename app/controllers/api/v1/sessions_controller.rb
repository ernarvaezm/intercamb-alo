module Api
  module V1
    class SessionsController < ApplicationController
     def create
      user = User.find_by(username: params[:username])
        if user.present? && user.authenticate(params[:password])
          @session = Session.new(session_params(user))
          if @session.present?
            @session.save
            render json: @session.auth_token
          else
            render json: @session.errors
          end
        else
          render json: 'Invalid Username or password', status:422
        end
     end

     def destroy
       session = Session.find_by(auth_token: params[:token])
       session.destroy
       head :no_content
     end

     private
     def generate_auth_token
       SecureRandom.uuid.gsub(/\-/,'')
     end

     def session_params(user)
       {user_id: user.id,
       auth_token: generate_auth_token
       }
     end
    end
  end
end
